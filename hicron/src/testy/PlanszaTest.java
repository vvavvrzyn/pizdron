package testy;

import static org.junit.Assert.*;
import hicron.Plansza;
import hicron.Pole;
import hicron.Ruch;
import hicron.Wilk;

public class PlanszaTest {
	
		
	@org.junit.Test
	public void dodajOwceTest() { 
		int x = 2; 
		int y = 3;  
		Plansza plansza = new Plansza(6); 
		plansza.setPolePlanszy(x, y, Pole.OWCA);  
		boolean wynik = plansza.getPole(x, y).equals(Pole.OWCA);
		assertEquals(true, wynik); 
	}
	@org.junit.Test(expected = Exception.class)
	public void dodajOwcePozaZakresTest() { 
		int x = 10; 
		int y = 10;  
		Plansza plansza = new Plansza(6); 
		plansza.setPolePlanszy(x, y, Pole.OWCA); 
	}
	@org.junit.Test
	public void ruchWilkaTest() { 
		int x=-1;
		int y=0;
		Plansza plansza = new Plansza(6); 
		Wilk wilk=new Wilk(plansza);
		wilk.ruch(new Ruch(x,y)); 
		boolean wynik = plansza.getPole((plansza.getRozmiar()-1)+x, y).equals(Pole.WILK);
		assertEquals(true, wynik); 
	}
	@org.junit.Test(expected = Exception.class)
	public void wyjdzWilkiemPozaZakresTest() {  
		int x=1;
		int y=1;
		Plansza plansza = new Plansza(6); 
		Wilk wilk=new Wilk(plansza);
		wilk.ruch(new Ruch(x,y));
	}
	@org.junit.Test
	public void czyGraZakonczonaTest() { 
		int x=4;
		int y=0;
		Plansza plansza = new Plansza(6);
		plansza.setPolePlanszy(x, y, Pole.OWCA);
		Wilk wilk=new Wilk(plansza);
		wilk.ruch(new Ruch(-1,0));
		assertEquals(true, plansza.isKoniecGry()); 
	}

}
