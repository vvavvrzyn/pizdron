package hicron;

/**
 * Klasa przewchowuj�ca zmian� po�o�enia Owcy/Wilka po akcji gracza.
 * 
 * @author Kamil Wawrzy�ski
 *
 */
public class Ruch {
	private int x,y;
	
	/**
	 * @param x zmiana po�o�enia w pionie
	 * @param y zmiana po�o�enia w poziomie
	 */
	public Ruch(int x,int y){
		this.x=x;
		this.y=y;
	}
	/**
	 * @return zwraca zmian� po�o�enia w pionie
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return zwraca zmian� po�o�enia w poziomie
	 */
	public int getY() {
		return y;
	}
	
}
