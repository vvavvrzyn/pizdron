package hicron;

import java.util.Scanner;

/**
 * Klasa realizuj�ca interfejs u�ytkownika oparty na konsoli.
 * 
 * @author Kamil Wawrzy�ski
 *
 */
public class InterfejsKonsolowy implements Interfejs {
	private boolean przerwijGre;
	
	@Override
	public void rozpocznijGre() {
		System.out.println("Witaj w grze Owca i Wilk!");
		System.out.println("Twoim zdaniem jest tak pokierowac wilkiem aby ten z�apa� owc�");
		System.out.println("Wilk porusza si� zgodnie z poni�szym schematem");
		System.out.println("1  2  3");
		System.out.println("4  W  6");
		System.out.println("7  8  9");
		System.out.println("W - aktualna pozycja wilka, poszczeg�lne cyfry przedstawiaj� kierunek, w kt�rym uda si� wilk");
		System.out.println("Aby przerwa� aktualn� parti� wci�nij: 0");
	}
	@Override
	public void wyswietl(Plansza plansza) {
		for(int i=0;i<plansza.getRozmiar();i++){
			for(int j=0;j<plansza.getRozmiar();j++){
				System.out.print("["+plansza.getPole(i, j)+"]");
			}
			System.out.println();
		}
		System.out.println("-----------------");
	}
	@Override
	public boolean czyGraszDalej() {
		if(!czyPrzerwacGre()){
			System.out.println("Owca zjedzona");
		}
		else{
			System.out.println("Przerwa�e� gr�.");
		}
		Scanner odczyt = new Scanner(System.in);
		System.out.println("Czy chcesz zagra� jeszcze raz? Je�li chcesz zako�czy� wpisz: n/N, w przeciwnym wypadku wpisz: t/T");
		String czyKoniec=odczyt.nextLine();
		
		return !czyKoniec.toUpperCase().equals("N");
	}

	@Override
	public Ruch getRuchGracza(Plansza plansza) {
		Ruch ruch;
		Scanner odczyt = new Scanner(System.in);
		int kierunekRuchu=odczyt.nextInt();
		ruch=zwrocZmianePolozeniaWilka(kierunekRuchu);
		przerwijGre(false);
		while(!czyMozeRuszyc(plansza, ruch)){
			System.out.println("Nie mo�esz tak ruszy�! Sprobuj ponownie.");
			kierunekRuchu=odczyt.nextInt();
			ruch=zwrocZmianePolozeniaWilka(kierunekRuchu);
		}
		if(kierunekRuchu==0){
			przerwijGre(true);
		}
		return ruch;
	}
	private boolean czyMozeRuszyc(Plansza plansza,Ruch ruch){
		

		return plansza.getWilkX()+ruch.getX()>=0 && 
				plansza.getWilkX()+ruch.getX()<plansza.getRozmiar() && 
				plansza.getWilkY()+ruch.getY()>=0 &&
				plansza.getWilkY()+ruch.getY()<plansza.getRozmiar();
	}
	private Ruch zwrocZmianePolozeniaWilka(int x){
		Ruch ruch;
		switch(x){
		case 1:
				ruch = new Ruch(-1,-1);
			break;
		case 2:
				ruch = new Ruch(-1,0);
			break;
		case 3:
				ruch = new Ruch(-1,1);
			break;
		case 4:
				ruch = new Ruch(0,-1);
			break;
		case 6:
				ruch = new Ruch(0,1);
			break;
		case 7:
				ruch = new Ruch(1,-1);
			break;
		case 8:
				ruch = new Ruch(1,0);
			break;
		case 9:
				ruch = new Ruch(1,1);
			break;
		default:
			ruch = new Ruch(0,0);
		}
		return ruch;
	}
	@Override
	public void zakonczGre() {
		System.out.println("Dzi�kuj� za gr�, do zobaczenia!");
	}
	@Override
	public boolean czyPrzerwacGre() {
		return przerwijGre;
	}
	private void przerwijGre(boolean przerwijGre) {
		this.przerwijGre=przerwijGre;
		
	}

}
