package hicron;

import java.util.Random;
/**
 * Klasa realizująca wyliczanie losowego ruchu dla owcy. 
 * 
 * @author Kamil Wawrzyński
 *
 */
public class LosowyAlgorytm implements Algorytm {
	private Random random=new Random();
	
	@Override
	public Ruch getRuch(Plansza plansza) {

		int x=random.nextInt(plansza.getRozmiar()); 
        int y=random.nextInt(plansza.getRozmiar()); 
        while(!(Math.abs(plansza.getOwcaX()-x)<=1 && 
                Math.abs(plansza.getOwcaY()-y)<=1 && 
                ((plansza.getOwcaX()-x)+(plansza.getOwcaY()-y))!=0)){ 
            x=random.nextInt(plansza.getRozmiar()); 
            y=random.nextInt(plansza.getRozmiar()); 
        } 
        return new Ruch(x, y);
	}
}
