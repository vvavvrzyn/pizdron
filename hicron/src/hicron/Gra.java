package hicron;

/**
 * Główna klasa uruchomieniowa programu
 * 
 * @author Kamil Wawrzyński
 *
 */
public class Gra {
	private Plansza plansza;
	private Wilk wilk;
	private Owca owca;
	private Algorytm algorytm;
	private Interfejs interfejs;
	private boolean czyPrzerwac;
	
	public static void main(String[] args) {
		Gra gra = new Gra(6, new LosowyAlgorytm(), new InterfejsKonsolowy());
		gra.start();
		}
	public Gra(int rozmiar, Algorytm algorytm, Interfejs interfejs){
		this.interfejs = interfejs;
		this.algorytm = algorytm;
		inicjalizuj(rozmiar); 
		}
	/**
	 * metoda rozpoczynająca rozgrywkę. 
	 */
	public void start() {
		interfejs.rozpocznijGre();
		
		boolean czyGrasz=false;
		while(!czyGrasz){
			interfejs.wyswietl(plansza);
		
			while(!koniecGry()){
				wilk.ruch(interfejs.getRuchGracza(plansza));
				czyPrzerwac=interfejs.czyPrzerwacGre();
				
				if(!koniecGry()){
					owca.ruch(algorytm.getRuch(plansza));
				}
				interfejs.wyswietl(plansza);
			}
			if(interfejs.czyGraszDalej()){
				restart();
			}
			else{
				czyGrasz=true;
			}
		}
		interfejs.zakonczGre();
		
	}
	private boolean koniecGry(){
		return plansza.isKoniecGry()||czyPrzerwac;
	}
	private void restart(){
		inicjalizuj(plansza.getRozmiar());
	}
	private void inicjalizuj(int rozmiar){
		plansza = new Plansza(rozmiar);
		this.owca=new Owca(plansza);
		this.wilk=new Wilk(plansza);
		czyPrzerwac=false;
		
	}
	

}
