package hicron;

/**
 * typ enum zawiera mo�liwe warto�ci dla pola planszy
 * 
 * @author Kamil Wawrzy�ski
 *
 */
public enum Pole {
		PUSTE(" "),
		OWCA("O"),
		WILK("W");
		
		private String pole;
		private Pole(String p) {
			pole=p;
		}
		public String toString(){
			return pole;
		}
}
