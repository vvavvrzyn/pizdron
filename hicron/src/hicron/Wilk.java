package hicron;

/**
 * Klasa implementująca postać Wilka w grze, steruje jego położeniem, 
 * umieszcza Wilka na planszy, wykonuje jego ruchy.
 * 
 * @author Kamil Wawrzyński
 *
 */
public class Wilk {
	private int x,y;
	private Plansza plansza;
	
	/**
	 * @param plansza plansza plansza na której rogrywana jest gra
	 */
	public Wilk(Plansza plansza){
		this.x=plansza.getRozmiar()-1;
		this.y=0;
		this.plansza=plansza;
		plansza.setPolePlanszy(x, y, Pole.WILK);
	}
	/**
	 * @return położenie w pionie Wilka
	 */
	public int getX(){
		return x;
	}
	/**
	 * @return położenie w poziomie Wilka
	 */
	public int getY(){
		return y;
	}
	/**
	 * @param x zadane położenie w pionie Wilka
	 */
	private void setX(int x){
		this.x=x;
	}
	/**
	 * @param y zadane położenie w poziomie Wilka
	 */
	private void setY(int y){
		this.y=y;
	}
	
	/**
	 * Metoda wykonująca ruch Wilka
	 * @param ruch obiekt klasy Ruch zawierający parametry zmiany położenia
	 */
	public void ruch(Ruch ruch){
		plansza.setPolePlanszy(getX(), getY(), Pole.PUSTE);
		setX(getX()+ruch.getX());
		setY(getY()+ruch.getY());
		plansza.setPolePlanszy(getX(), getY(), Pole.WILK);
		
	}
}
