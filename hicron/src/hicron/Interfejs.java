package hicron;

public interface Interfejs {
	
	/**
	 * wy�wietla komunikat powitalny oraz instrukcj� do gry
	 */
	public void rozpocznijGre();
	/** 
	 * wyswietla plansz� gry
	 * @param plansza plansza na kt�rej rogrywana jest gra
	 */
	public void wyswietl(Plansza plansza);
	/**
	 * @return true je�li gracz chce ponownie rozegra� parti� gry, w przeciwnym wypadku false
	 */
	public boolean czyGraszDalej();
	/**
	 * @return true je�li gracz chce przewa� bie��c� parti�, w przeciwnym wypadku false
	 */
	public boolean czyPrzerwacGre();
	/**
	 * obs�uguje zako�czenie gry
	 */
	public void zakonczGre();
	/**
	 * @param plansza plansza na kt�rej rogrywana jest gra
	 * @return zwraca obiekt klasy Ruch zawieraj�cego parametry zmiany po�o�enia dla wilka
	 */
	public Ruch getRuchGracza(Plansza plansza);
}
