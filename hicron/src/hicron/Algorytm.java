package hicron;

public interface Algorytm {
	
	/**
	 * @param plansza plansza na której rogrywana jest gra
	 * @return zwraca obiekt klasy Ruch zawierającego parametry zmiany położenia dla owcy
	 */
	public Ruch getRuch(Plansza plansza);
}
