package hicron;

/**
 * Klasa realizuj�ca implementacj� planszy do gry
 * 
 * @author Kamil Wawrzy�ski
 *
 */
public class Plansza {
	private Pole[][] plansza;

	 /**
	 * @param rozmiar rozmiar planszy do gry
	 */
	public Plansza(int rozmiar){
		plansza=new Pole[rozmiar][rozmiar];
		for(int i = 0;i<plansza.length;i++){
			for(int j=0;j<plansza[0].length;j++){
				plansza[i][j]=Pole.PUSTE;
			}
		}
	}
	/**
	 * @return po�o�enie w pionie Owcy na planszy
	 */
	public int getOwcaX(){
		return zwrocX(Pole.OWCA);
	}
	/**
	 * @return po�o�enie w pionie Owcy na planszy
	 */
	public int getOwcaY(){
		return zwrocY(Pole.OWCA);
	}
	/**
	 * @return po�o�enie w pionie Wilka na planszy
	 */
	public int getWilkX(){
		return zwrocX(Pole.WILK);
	}
	/**
	 * @return po�o�enie w pionie Wilka na planszy
	 */
	public int getWilkY(){
		return zwrocY(Pole.WILK);
	}
	/**
	 * @return rozmiar planszy
	 */
	public int getRozmiar(){
		return plansza.length;
	}
	/**
	 * Metoda zwraca warto�� pola w podanym przez u�ytkownika miejscu planszy
	 * @param x po�o�enie w pionie
	 * @param y po�o�enie w poziomie
	 * @return enum Pole na wskazanym miejscu planszy, gdy podane miejsce jest 
	 * poza zakresem planszy wyrzucany jest wyj�tek 
	 */
	public Pole getPole(int x, int y){
		if(x>=0 && x<getRozmiar() && y>=0 && y<getRozmiar()){
			return plansza[x][y];
		}
		else{
			throw new IllegalArgumentException("parametry poza zakresem planszy"); 
		}
	}
	/**
	 * Metoda ustawia warto�� pola w podamym przez u�ytkownika miejscu planszy.
	 * Je�li zadane przez u�ytkownika parametry nie znajduj� w zakresie planszy, zostanie wyrzucony wyj�tek.
	 * @param x po�o�enie w pionie
	 * @param y po�o�enie w poziomie
	 * @param pole enum Pole 
	 */
	public void setPolePlanszy(int x, int y,Pole pole){
		if(x>=0 && x<getRozmiar() && y>=0 && y<getRozmiar()){
			plansza[x][y]=pole;
		}
		else{
			throw new IllegalArgumentException("parametry poza zakresem planszy"); 
		}
	}

	/**
	 * @return true je�li na planszy nie ma ju� Owcy, w przeciwnym wypadku false
	 */
	public boolean isKoniecGry(){
		for(int i=0;i<getRozmiar();i++){
			for(int j=0;j<getRozmiar();j++){
				if(getPole(i, j).equals(Pole.OWCA)){
					return false;
				}
			}
		}
		
		return true;
	}
	private int zwrocX(Pole pole){
		for(int i=0;i<getRozmiar();i++){
			for(int j=0;j<getRozmiar();j++){
				if(getPole(i, j).equals(pole)){
					return i;
				}
			}
		}
		return 0;
	}
	private int zwrocY(Pole pole){
		for(int i=0;i<getRozmiar();i++){
			for(int j=0;j<getRozmiar();j++){
				if(getPole(i, j).equals(pole)){
					return j;
				}
			}
		}
		return 0;
	}
}
