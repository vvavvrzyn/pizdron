package hicron;

import java.util.Random;

/**
 * Klasa implementuj�ca posta� Owcy w grze, steruje jej po�o�eniem, 
 * umieszcza Owc� na planszy, wykonuje ruchy Owcy
 * 
 * @author Kamil Wawrzy�ski
 *
 */
public class Owca {
	private int x,y;
	private Plansza plansza;
	
	/**
	 * @param plansza plansza na kt�rej rogrywana jest gra
	 */
	public Owca(Plansza plansza){
		
		Random random=new Random();
		int rozmiarPlanszy=plansza.getRozmiar();
		int x=random.nextInt(rozmiarPlanszy-1);
		int y=random.nextInt(rozmiarPlanszy-1)+1;
		
		this.x=x;
		this.y=y;
		this.plansza=plansza;
		plansza.setPolePlanszy(x, y, Pole.OWCA);
		
	}
	/**
	 * @return po�o�enie w pionie Owcy
	 */
	public int getX(){
		return x;
	}
	/**
	 * @return po�o�enie w poziomie Owcy
	 */
	public int getY(){
		return y;
	}
	/**
	 * @param x zadane po�o�enie w pionie Owcy
	 */
	private void setX(int x){
		this.x=x;
	}
	/**
	 * @param y zadane po�o�enie w poziomie Owcy
	 */
	private void setY(int y){
		this.y=y;
	}

	/**
	 * Metoda wykonuj�ca ruch Owcy
	 * @param ruch obiekt klasy Ruch zawieraj�cy parametry zmiany po�o�enia
	 */
	public void ruch(Ruch ruch){
		plansza.setPolePlanszy(getX(), getY(), Pole.PUSTE);
		setX(ruch.getX());
		setY(ruch.getY());
		
		if(!plansza.getPole(getX(), getY()).equals(Pole.WILK)){ 
            plansza.setPolePlanszy(getX(), getY(), Pole.OWCA); 
        }
		
	}

}
